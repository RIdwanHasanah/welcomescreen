package com.indonesia.ridwan.welcomscreen;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

public class MainActivity extends AppCompatActivity {

    RelativeLayout introMessage;
    LinearLayout appContent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button b = (Button) findViewById(R.id.button);

        b.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this,Main2Activity.class);
                startActivity(i);

            }
        });

        introMessage = (RelativeLayout) findViewById(R.id.welcomepesan);

        appContent = (LinearLayout) findViewById(R.id.contentlayout);
    }

    public void dismisWelcomeMesssageBox (View view){

        introMessage.setVisibility(View.INVISIBLE);
        appContent.setVisibility(View.VISIBLE);
    }
}
